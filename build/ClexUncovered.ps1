# ESEGUIRE DA LINEA DI COMANDO:
# Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass

# Costanti
new-variable -name PackageName -value "ClexUncovered2"
new-variable -name PackageFold -value "..\release"
new-variable -name PackageFile -value "$PackageFold\$PackageName.xml"
new-variable -name PackageXML -value "uc-aliases.xml","uc-triggers.xml","uc-scripts.xml"
new-variable -name PackageTAG -value "AliasPackage","TriggerPackage","ScriptPackage"
# Variabili temporanee
new-variable -name UIcontent -value ""
new-variable -name UIfile -value ""

# Verifico se la versione di powershell � sufficiente
if ($PSVersionTable.PSVersion.Major -lt 5) {
	echo "Questo script pu� essere eseguito solo da PowerShell 5 o superiore"
	exit
}

# Pulisco la vecchia build
if (Test-Path -Path "$PackageFold") { Remove-Item -LiteralPath "$PackageFold" -Force -Recurse }

# Copio i file statici
Copy-Item "..\resource\" -Destination "$PackageFold\resource\" -Recurse -Force -Container
Copy-Item "..\config.lua" -Destination "$PackageFold\config.lua" -Force
Copy-Item "master.xml" -Destination "$PackageFile" -Force

# Copio i contenuti dei file xml
for ($counter=0; $counter -lt $PackageXML.Length; $counter++){
	$UIfile = Get-Item "..\$($packageXML[$counter])"
	$UIcontent = ([IO.File]::ReadAllText($UIfile.FullName, [System.Text.Encoding]::UTF8))
	
	if ($UIcontent -Match "(?smi)(<" + $PackageTAG[$counter] + ">.*<\/" + $PackageTAG[$counter] + ">)") {  
		$saveme = $matches[1] 
	} else {
		echo "Impossibile trovare >>>$($PackageTAG[$counter])<<< nel file master!"
		exit
	}
	(Get-Content $PackageFile -Encoding UTF8) -replace "<$($PackageTAG[$counter]) \/>", $saveme | Set-Content $PackageFile -Encoding UTF8
}

#Compress-Archive -Path "$PackageFold\*" -CompressionLevel Optimal -DestinationPath "$PackageFold\$PackageName.zip"
#Rename-Item -Path "$PackageFold\$PackageName.zip" -NewName "$PackageName.mpackage"

Read-Host -Prompt "Press Enter to continue"